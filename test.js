import test from 'ava';
import task from './lib/task.js'

test('task', async t => {
  t.plan(7)

  // normal case
  const wait_for = v => new Promise(resolve => {
    setTimeout(() => resolve(v))
  })
  let values = await task(async function() {
    let v = []
    v.push(await wait_for(1))
    setTimeout(() => {
      this.continue(2)
    }, 10)
    v.push(await this.wait())
    return await task(async function() {
      return [...v, 3, 4]
    })
  })
	t.deepEqual(values, [1, 2, 3, 4]);

  // continue_cb
  function resolve_fn(should_fail, fn) {
    if (!fn) {
      throw Error('not a fn')
    }
    setImmediate(function() {
      if (should_fail) {
        fn(new Error('An error happened'), null)
      } else {
        fn(null, 'a success value')
      }
    })
  }
  let continue_success_v = await task(async function() {
    resolve_fn(false, this.continue_cb)
    return await this.wait()
  })
  t.deepEqual(continue_success_v, 'a success value')
  let continue_throw_v = task(async function() {
    resolve_fn(true, this.continue_cb)
    return await this.wait()
  })
  t.throws(continue_throw_v)

  // warn about wait not being called yet
  t.throws(task(async function() {
    this.continue()
  }), /\.wait\(\) has not been called/)

  // warn about passing task.continue instead of task.continue_cb
  function test_unbound_continue(cb, actually_done) {
    setImmediate(() => {
      t.throws(() => {
        cb()
      }, /instead of 'this\.continue_cb'/)
      actually_done()
    })
  }
  await task(async function() {
    test_unbound_continue(this.continue, () => {
      this.continue()
    })
    return await this.wait()
  })

  // catch
  let fail_v = await task(async function() {
    this.catch(e => {
      return e.value + 10
    })
    let e = new Error('Fail value')
    e.value = 1
    throw e
  })
  t.is(fail_v, 11)

  // finally
  let finally_v = []
  await task(async function() {
    this.catch(() => finally_v.push(1))
    this.finally(() => finally_v.push(2))
    this.finally(() => finally_v.push(3))
    throw new Error('Testing finally')
  })

  // sleep
  await task(async function() {
    let start = Date.now()
    await task.sleep(100)
    let time_elapsed = Date.now() - start
    t.true(time_elapsed >= 100)
  })
});
