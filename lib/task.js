module.exports = task
const ERR_NO_UNBOUND = "task: callback is not bound correctly. Are you passing 'this.continue' as a callback instead of 'this.continue_cb'?"
const ERR_NO_WAITER = 'task: cannot continue/throw, .wait() has not been called yet'

async function task(fn) {
  let context = {
    _catch: null,
    catch(fn) { this._catch = fn },
    _wait: null,
    _ensure_wait() {
      if (!this._wait) {
        throw new Error(ERR_NO_WAITER)
      }
    },
    wait() {
      return new Promise((resolve, reject) =>
        this._wait = {resolve, reject})
    },
    _finally: [],
    finally(fn) { this._finally.push(fn) },
  }
  context.throw = function(err) {
    if (!this._ensure_wait) {
      throw new Error(ERR_NO_UNBOUND)
    }
    this._ensure_wait()
    this._wait.reject(err)
  }
  context.continue = function(value) {
    if (!this._ensure_wait) {
      throw new Error(ERR_NO_UNBOUND)
    }
    this._ensure_wait()
    this._wait.resolve(value)
  }
  context.continue_cb = (err, value) => {
    if (err) {
      context.throw(err)
    } else {
      context.continue(value)
    }
  }
  try {
    return await fn.call(context)
  } catch(err) {
    if (context._catch) {
      return await context._catch(err)
    } else {
      throw err
    }
  } finally {
    context._finally.forEach(fn => fn())
  }
}

task.sleep = function task_sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}
